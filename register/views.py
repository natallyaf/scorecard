from django.shortcuts import render
from register.models import Registration

# Create your views here.
# Create your views here.
def mostrar_index(request):
   return render(request, "index.html", {})

def render_registration_done(request):
   registration_data = {
      'cxKpiName': request.POST.get('cxKpiName'),
      'cxRegion': request.POST.get('cxRegion'),
      'cxBusiness': request.POST.get('cxBusiness'),
      'cxPeriod': request.POST.get('cxPeriod'),
      'cxYTDQ1': request.POST.get('cxYTDQ1'),
      'cxYTDQ2': request.POST.get('cxYTDQ2'),
      'cxYTDQ3': request.POST.get('cxYTDQ3'),
      'cxYTDQ4': request.POST.get('cxYTDQ4'),
      'cxTGTQ1': request.POST.get('cxTGTQ1'),
      'cxTGTQ2': request.POST.get('cxTGTQ2'),
      'cxTGTQ3': request.POST.get('cxTGTQ3'),
      'cxTGTQ4': request.POST.get('cxTGTQ4')
   }
   registration = Registration(**registration_data)
   registration.save()

   return render(request, 'registration_done.html', {'registration_data': registration})
