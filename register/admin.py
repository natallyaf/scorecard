from typing import Tuple

from django.contrib import admin
from .models import Registration


class RegistrationAdmin(admin.ModelAdmin):
    date_hierarchy = "registration_date"
    list_display: ("cxKpiName", "cxRegion", "registration_date")
    list_filter = ("cxKpiName", "cxRegion")
    search_fields = ("cxKpiName", "cxRegion")

admin.site.register(Registration, RegistrationAdmin)
