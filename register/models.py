from django.db import models

# Create your models here.
class Registration(models.Model):
    cxKpiName = models.CharField(max_length=30)
    cxRegion = models.CharField(max_length=20)
    cxBusiness = models.CharField(max_length=20)
    cxPeriod = models.CharField(max_length=5)
    cxYTDQ1 = models.CharField(max_length=4)
    cxYTDQ2 = models.CharField(max_length=4)
    cxYTDQ3 = models.CharField(max_length=4)
    cxYTDQ4 = models.CharField(max_length=4)
    cxTGTQ1 = models.CharField(max_length=4)
    cxTGTQ2 = models.CharField(max_length=4)
    cxTGTQ3 = models.CharField(max_length=4)
    cxTGTQ4 = models.CharField(max_length=4)
    registration_date = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
